library(shiny)

shinyUI(fluidPage(
  titlePanel("Analysis of Stock Price Over Time with Sentiment Correlation"),
  
  sidebarLayout(
    sidebarPanel(
      helpText("Please enter a company from Google finance."),
    
      textInput("symb", "Symbol", "AAPL"),
    
      dateRangeInput("dates", 
        "Date range",
        start = "2017-05-15", 
        end = "2017-05-31"),
      
      br(),
      
      selectInput("var", "Stock Price Information:", 
                  choices=c("Open","Close","High","Low","Close","Daily Return","Daily Log Return")),
      selectInput("sen", "Sentiment Information:", 
                  choices=c("Volume","Sentiment")),
      selectInput("sent", "Sentiment Type:", 
                  choices=c("All","High","Medium")),
      helpText("Please enter sentiment data sources."),
      textInput("db1", "Database1", "infotrie_crawler"),
      textInput("co1", "Collection1", "daily_sentiment"),
      textInput("db2", "Database2", "infotrie_crawler2"),
      textInput("co2", "Collection2", "random_daily_sentiment"),
      hr()
      
    ),
    
    mainPanel(
      tags$head(tags$style(type="text/css", "
             #loadmessage {
                           position: fixed;
                           top: 0px;
                           left: 0px;
                           width: 100%;
                           padding: 5px 0px 5px 0px;
                           text-align: center;
                           font-weight: bold;
                           font-size: 100%;
                           color: #000000;
                           background-color: #CCFF66;
                           z-index: 105;
                           }
                           ")),
      plotlyOutput("plot"),
      br(),
      br(),
      h4("VAR(p=2) Model Evaluation Summary"),
      htmlOutput( "Text"),
      textOutput("Text0"),
      tableOutput("sum"),
      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                       tags$div("Loading...",id="loadmessage"))
      
  )
)))