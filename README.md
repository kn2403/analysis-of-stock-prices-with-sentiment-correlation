#Project Introduction#
R Shiny Application that visualizes stock and sentiment information and interacts with users.

#Current Functions#
-Look up stock prices over years

-Point out the errors of user input

-Select the variable and display its visualization (Daily Stock Close Price, Open Price, High Price, Low Price, Return Rate and Return Log Price)

-Select sentiment values or volume of different types and display them on the graph. Fit the sentiment and stock information into time series model with prediction values. Also display the test statistics for the model.

-Compare two set of sentiment data from two collections. According to the comparison of test statistics, choose the better data set.

#Instruction #
Simply open server.r and ui.r in R and run the application, type in a stock name and set the time frame, choose the variables from dropdown menu. Type in database and collection names.