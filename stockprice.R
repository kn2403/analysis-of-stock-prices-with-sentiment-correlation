data <- rownames_to_column(data.frame(predata))
data$rowname<-as.Date(data$rowname)
colnames(data)<-c("Date","Open","High","Low","Close","volume")
length<-as.numeric(max(data$Date)-min(data$Date))
n<-ifelse(length<20,1,length%/%20)
sep<-paste(n,"days")

data$DailyReturn<-Delt(data$Close)
data$logDailyReturn<-Delt(data$Close,type='log')

if(input$var=="Daily Return"){ col<-"DailyReturn"

}else if(input$var=="Daily Log Return"){ col<-"logDailyReturn"

}else{col<-input$var}

data<-data.frame(cbind(data[,'Date'],data[,col]))
colnames(data)<-c("date","y")
data$date<-as.Date(data$date)
data$y[which(is.na(data$y)==TRUE)]<-0
